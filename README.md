# Bias in Immersive Virtual Space to Think

In this project, we examined participants two types of behavioral pattern in an immersive space to think experiment. We analysized:

  - Two aspects of users' movement
    - total movement in the workspace (`./handedness.ipynb`)
    - timespent across the workspace (`./explore.ipynb`)
  - Five aspects of users' interaction (grabbing behavior)
    - Initial interaction (`./explore.ipynb`)
    - Consecutive Interaction (`./explore.ipynb`)
    - Interaction Frequency (`./HeatmapsForBias/bias.ipynb`)
    - Distance during interaction (`./explore.ipynb`)

### Requirements
You need couple of standard packages for python(3.6+) including:

* Jupyter Lab ([conda-forge/jupyterlab on Anaconda Cloud](https://anaconda.org/conda-forge/jupyterlab))
* Pandas ([conda-forge/pandas](https://anaconda.org/conda-forge/pandas))
* Numpy ([conda-forge/numpy](https://anaconda.org/conda-forge/numpy))
* Scipy ([conda-forge/scipy](https://anaconda.org/conda-forge/scipy))
* Matplotlib ([conda-forge/matplotlib](https://anaconda.org/conda-forge/matplotlib))
* Plotly ([conda-forge/plotly](https://anaconda.org/conda-forge/plotly))
* Seaborn ([conda-forge/seaborn](https://anaconda.org/conda-forge/seaborn))
* Statsmodels ([conda-forge/statsmodels](https://anaconda.org/conda-forge/statsmodels))
* Scikit-learn ([conda-forge/scikit-learn](https://anaconda.org/conda-forge/scikit-learn))
* Pylcs ([pip:pylcs](https://pypi.org/project/pylcs/))

In addition, there are couple of packages you need for python(2.7) files (.py) including:

* hawkeslib
* traclus_impl
* python-rtree

When in doubt, check out ```requirements.txt```

### Setup Procedure

```sh
$ git clone git@bitbucket.org:nomem/ve-bias.git
$ cd ve-bias
$ conda create -n veproject python=3.7
$ conda activate veproject
$ conda install --file requirements.txt
$ # voila, do your thing
$ # for python=2.7, create another environment
```
### Note
We may not upload some of the data file due to IRB requirements. If you have a question, email: momen@vt.edu or kyliedavidson@vt.edu or ge@vt.edu
