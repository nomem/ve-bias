# python2 ~/.local/lib/python2.7/site-packages/traclus_impl/main.py -i data/movement/grab_traclus.json -o data/movement/grab_traclus_output.json

import pandas
import json

df_first_action = pandas.read_csv('data/movement/first_action.csv')
df_doc = pandas.read_csv('HeatmapsForBias/UnmovedDocumentTotals.csv')

df_doc.rename(columns={'Document': 'doc'}, inplace=True)
df_doc['x'] = 2.50
# revert what kylie did
df_doc['z'] = -df_doc['z']
df_doc['doc'] = df_doc['doc'].map(lambda x: x.split('.')[0])

config = {"epsilon": 0.26, "min_neighbors": 4, "min_num_trajectories_in_cluster": 3, "min_vertical_lines": 2, "min_prev_dist": 0.0002}
trajectories = []
for user in df_first_action['user'].unique():
    if user == 21 or user == 23:
        continue
    tmp_doc_order = df_first_action.loc[(df_first_action['action']=='grab') & (df_first_action['user']==user)].sort_values(by=['time'])['doc'].tolist()
    tmp_doc_order = [doc for doc in tmp_doc_order if "/" in doc and "Copy" not in doc]
    pos_x = [df_doc.loc[df_doc['doc']==doc]['z'].tolist()[0] for doc in tmp_doc_order]
    pos_y = [df_doc.loc[df_doc['doc']==doc]['y'].tolist()[0] for doc in tmp_doc_order]
    trajectories.append([{"x": float("%.4f"%pos_x[i]), "y": float("%.4f"%pos_y[i])} for i in range(len(pos_x))])

config["trajectories"] = trajectories

with open('data/movement/grab_traclus.json', 'w') as outfile:
    json.dump(config, outfile)