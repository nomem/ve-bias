import pandas
import re
from os import listdir
from os.path import isfile, join

log_dir = 'data/action'
user = []
time = []
pos_x = []
pos_y = []
pos_z = []

for file in [f for f in listdir(log_dir) if isfile(join(log_dir, f))]:
    f = open(join(log_dir, file))
    tmpuser = re.findall(r'\d+', file)[0]
    for line in f:
        data = line.split(",")
        if len(data) < 2:
            continue
        if data[1] == 'camera':
            user.append(tmpuser)
            time.append(data[0])
            pos_x.append(data[2])
            pos_y.append(data[3])
            pos_z.append(data[4])
df = pandas.DataFrame({
    'user': user,
    'time': time,
    'pos_x': pos_x,
    'pos_y': pos_y,
    'pos_z': pos_z
})
df.to_csv('data/movement/camera.csv', index=None)
        
