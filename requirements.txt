jupyter
jupyterlab==1.2
ipywidgets>=7.5
pandas
numpy
scipy
matplotlib
seaborn
plotly
plotly_express
scikit-learn
# hawkeslib python 2
# traclus_impl TraClus python 2
# for rtree do sudo apt install libspatialindex-dev python-rtree
# pylcs
hmmlearn