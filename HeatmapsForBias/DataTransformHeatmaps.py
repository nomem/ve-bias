import sys
import os,  fnmatch
import pandas as pd



def main():

    #Get all of the layout files
    path = '/Users/kyliedavidson/OneDrive - Virginia Tech/VirginiaTech/LAS/ISTPlots/Layouts/'
    allLayouts = fnmatch.filter(os.listdir(path), '*.csv')

    mapWithCounts = {}
    val = 2.50
    coordinates = {}

    for i in range (0, len(allLayouts)):
        data = pd.read_csv(path + allLayouts[i], usecols = [0,1, 2,3], header = None)
        data.columns = ['Document', 'x', 'y','z']
        #on first time through initialize a new dataframe with document, counter where the counter is number of times that the document was at x = 2.50

        for index, row in data.iterrows():
            documentName = row[0]
            #print("Value of row at 1 is : " , row[1])

            # print (row[1] == val)


            if row[1] == 2.50 and 'Label' not in documentName:
                if documentName in mapWithCounts:
                    mapWithCounts[documentName] = mapWithCounts[documentName] + 1

                else:
                    mapWithCounts[documentName] =  1
                    coordinates[documentName] = str(row[2]) + ' ' + str(row[3])
                    print(coordinates[documentName])

    #print(mapWithCounts)
    #print(coordinates)

    # print("Length of the map is    : ", len(mapWithCounts))
    countsOfUnmovedDocuments = pd.DataFrame.from_dict(mapWithCounts.items())
    countsOfUnmovedDocuments.columns = ['Document', 'counts']
    coordinatesOfUnmovedDocuments = pd.DataFrame.from_dict(coordinates.items())
    coordinatesOfUnmovedDocuments.columns = ['Document', 'coords']
    coordinatesOfUnmovedDocuments[['y','z']] = coordinatesOfUnmovedDocuments['coords'].str.split(expand=True)

    #dataframeWithCoordsAndCounts = pd.concat([coordinatesOfUnmovedDocuments, countsOfUnmovedDocuments], ignore_index=True)

    #dataframeWithCoordsAndCounts = pd.concat([coordinatesOfUnmovedDocuments, countsOfUnmovedDocuments], ignore_index=True)#,  sort=True)

    newFrame = countsOfUnmovedDocuments.merge(coordinatesOfUnmovedDocuments, on='Document', how='left')

    newFrame = newFrame.drop(['coords'], axis=1)

    print("New Data Frame")
    print(newFrame.head())
    newFrame = newFrame.astype({"z": float})
    newFrame["z"] = -1.0 * newFrame["z"]
    print(newFrame.dtypes)
    print(newFrame.head())

    # print(countsOfUnmovedDocuments.head())
    # print()
    # print(coordinatesOfUnmovedDocuments.head())

    document_to_write_to = 'UnmovedDocumentTotals.csv'
    newFrame.to_csv(document_to_write_to, header=True, index=False)

    #for each layout in layouts folder
        #find each document's x value = 2.50
            #if the x value is 2.50
                #increment the counter on that file in a csv










if __name__ == "__main__":
    main()
