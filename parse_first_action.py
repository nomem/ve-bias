import pandas
import re
from os import listdir
from os.path import isfile, join

log_dir = 'data/action'

hoverdata = {}
grabdata = {}
for i in range(25):
    hoverdata[str(i)] = {}
    grabdata[str(i)] = {}

for file in [f for f in listdir(log_dir) if isfile(join(log_dir, f))]:
    f = open(join(log_dir, file))
    tmpuser = re.findall(r'\d+', file)[0]
    for line in f:
        line = line.strip()
        data = line.split(",")
        if len(data) < 2:
            continue
        if data[1] == 'startHover':
            time = float(data[0])
            doc = data[2]
            if doc in hoverdata[tmpuser].keys():
                if time < hoverdata[tmpuser][doc]:
                    hoverdata[tmpuser][doc] = time
            else:
                hoverdata[tmpuser][doc] = time
        if data[1] == 'startGrab':
            time = float(data[0])
            doc = data[2]
            if doc in grabdata[tmpuser].keys():
                if time < grabdata[tmpuser][doc]:
                    grabdata[tmpuser][doc] = time
            else:
                grabdata[tmpuser][doc] = time


user = []
time = []
doc = []
action = []

for u in hoverdata.keys():
    for d in hoverdata[u].keys():
        user.append(u)
        doc.append(d)
        time.append(hoverdata[u][d])
        action.append("hover")


for u in grabdata.keys():
    for d in grabdata[u].keys():
        user.append(u)
        doc.append(d)
        time.append(grabdata[u][d])
        action.append("grab")

df = pandas.DataFrame({
    'user': user,
    'time': time,
    'doc': doc,
    'action': action
})

df.to_csv('data/movement/first_action.csv', index=None)
        
