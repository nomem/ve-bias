# python2 ~/.local/lib/python2.7/site-packages/traclus_impl/main.py -i data/movement/camera_traclus.json -o data/movement/camera_traclus_output.json

import pandas
import json

df = pandas.read_csv('data/movement/camera_hawkes.csv')

config = {"epsilon": 0.016, "min_neighbors": 4, "min_num_trajectories_in_cluster": 3, "min_vertical_lines": 2, "min_prev_dist": 0.0002}
trajectories = []
for user in df['user'].unique():
    if user == 21 or user == 23:
        continue
    df_t = df.loc[df['user'] == user]
    pos_x = df_t['pos_x'].tolist()
    pos_y = df_t['pos_z'].tolist()
    trajectories.append([{"x": float("%.4f"%pos_x[i]), "y": float("%.4f"%pos_y[i])} for i in range(len(pos_x))])

config["trajectories"] = trajectories

with open('data/movement/camera_traclus.json', 'w') as outfile:
    json.dump(config, outfile)