# from gensim.utils import simple_preprocess
# from gensim.parsing.preprocessing import STOPWORDS
# from nltk.stem import WordNetLemmatizer, SnowballStemmer
# from nltk.stem.porter import *
# import numpy as np
import pandas as pd

def main():

    from collections import Counter


    prompt1 = "According to the soldiers who responded, what ought to be the overriding consideration in the Allied war effort, pragmatism or principle?  Should America be fighting for the principles of democracy wherever those principles are threatened or violated, even in America itself; or should the country focus solely on winning the war to end it as soon as possible?  "
    prompt2 = "How do these views differ based on the respondent’s racial identification? What do they indicate about the state of race relations across the armed forces?"
    promptIntro = "You are about to read a selection of anonymous, open-ended, handwritten responses from White and Black WWII soldiers who were surveyed by the US Army in March 1943.  Five keywords were used to select and organized this transcribed sample:  fight, country, negro, white, and fair.  The purpose of the Army’s 'Survey 32' was to compare the attitudes of 'Negro' enlisted men with those of white enlisted men.  All respondents from 32N were identified as Black, and all respondents from 32W as White.  The US Army wanted to study this divisive issue because, on the one hand, Jim Crow segregation was widely adhered to in 1940s America, including the US Army itself.  Yet on the other hand, the US Army needed as many men in the fight as possible. They needed Black males of enlistment age to help wage America’s war abroad. With this in mind, your task is to analyze a sufficient number of documents to be able answer the following questions about the state of race relations in the US Army during WWII.  You need not read every document but enough to feel secure in your analysis.  The second question is comparative, so be sure to read a broad selection of responses across both 32W and 32N."

    allPrompts = prompt1 + prompt2 + promptIntro

    from nltk.corpus import stopwords
    from nltk.tokenize import word_tokenize

    example_sent = "This is a sample sentence, showing off the stop words filtration."

    stop_words = set(stopwords.words('english'))

    word_tokens = word_tokenize(prompt1)
    word_tokens = [word for word in word_tokens if word.isalpha()]
    word_tokens = [w for w in word_tokens if not w in stop_words]
    # stemming of words
    from nltk.stem.porter import PorterStemmer
    porter = PorterStemmer()
    word_tokens = [porter.stem(word) for word in word_tokens]

    filtered_sentence = []

    for w in word_tokens:
        if w not in stop_words:
            filtered_sentence.append(w)

    filtered_sentence = [word.lower() for word in filtered_sentence]


    counts = Counter(filtered_sentence)
    counts = dict(counts)


    #write the python dict to a csv for each prompt, the for all prompts togther
    csv_file = "Prompt1TextAnalysis.csv"
    (pd.DataFrame.from_dict(data=counts, orient='index').to_csv(csv_file, header=False))



if __name__ == "__main__":
    main()
