﻿using System;
using System.Numerics;

public class DocumentTracker {
	// name of tracked document
	private string name_;
	public string name {
		get { return name_; }
		set { name_ = value; }
	}

	private int grabCount_;
	public int grabCount {
		get { return grabCount_; }
		set { grabCount_ = value; }
	}

	// distance the document has traveled
	private double distTraveled_;
	public double distTraveled { get { return distTraveled_; } }

	// last known position of the document
	private Vector3 curPosition;

	// largest known size of the document
	private Vector3 largestSize_;
	public Vector3 largestSize { get { return largestSize_; } }


	public DocumentTracker(string[] initLog) {
		distTraveled_ = 0f;

		// error checking
		if (initLog.Length != 13) {
			Console.WriteLine("error in initializing document of indeterminate name");
			return;
		}


		// set name
		name_ = initLog[2];
		// setup the first reported position
		curPosition = new Vector3(float.Parse(initLog[3]), float.Parse(initLog[4]),
			float.Parse(initLog[5]));

		// record the largest size
		largestSize_ = new Vector3(float.Parse(initLog[10]), float.Parse(initLog[11]),
			float.Parse(initLog[12]));
	}

	public void Update(string[] log) {
		if (log.Length != 13) {
			Console.WriteLine("error in updating document: " + name_);
			return;
		}

		Vector3 nextPosition = new Vector3(float.Parse(log[3]), float.Parse(log[4]),
			float.Parse(log[5]));

		// something weird happens occasionally where there's an absurdly
		// large number, this prevents that from happening
		double temp = Vector3.Distance(curPosition, nextPosition);
		if (temp < 10f) {
			distTraveled_ += temp;
			curPosition = nextPosition;
		}
		// print an error log though, just to check values
		else {
			string tempstr = "";
			for (int i = 0; i < 13; i++) {
				tempstr += log[i] + ",";
			}
			Console.WriteLine("distance between positions is too great: " +
				"\n\tCur position: " + curPosition.ToString("0.00") +
				"\n\tDistance to next position: " + temp + ",\n\t" + tempstr);
		}

		// check to see if it's larger than previous record
		Vector3 nextSize = new Vector3(float.Parse(log[10]), float.Parse(log[11]),
			float.Parse(log[12]));

		if (nextSize.X > largestSize_.X) largestSize_ = nextSize;
	}
}
