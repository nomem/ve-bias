﻿using System;
using System.IO;
using System.Collections.Generic;

public class LogFile {
	// name of the file
	private string name_;
	public string name {
		get { return name_; }
	}

	// number of commands in file
	public int comCount {
		get { return comCount; }
	}

	// number of times user hovered on a doc
	private int hoverCount_;
	public int hoverCount {
		get { return hoverCount_; }
	}

	// number of times user started a highlight on a doc
	private int highlightCount_;
	public int highlightCount {
		get { return highlightCount_; }
	}

	// number of times a user permanently highlighted
	private int permHighCount_;
	public int permHighCount {
		get { return permHighCount_; }
	}

	// number of times a user utilized search
	private int searchCount_;
	public int searchCount {
		get { return searchCount_; }
	}

	// List of documents the user copied
	private List<string> copyList_;
	public List<string> copyList {
		get { return copyList_; }
	}

	// all highlights this user selected
	// TODO: maybe include the document it was associated with?
	private List<string> permHighlights_;
	public List<string> permHighlights {
		get { return permHighlights_; }
	}

	public List<DocumentTracker> docs_;
	public List<DocumentTracker> docs {
		get { return docs_; }
	}

	public CamTracker camera;

	static public List<string> newComs;

	public LogFile(FileInfo inFile) {
		if (newComs == null) newComs = new List<string>();

		name_ = inFile.Name;
		StreamReader logFile = new StreamReader(inFile.FullName);

		string logLine = "";
		int counter = 0;
		while((logLine = logFile.ReadLine()) != null) {

			//Console.WriteLine(logLine);
			// command counter
			counter++;

			// split for various command parameters
			string[] comSplit = logLine.Split(",");

			// error checking
			if (comSplit.Length < 2) {
				Console.WriteLine("Error in log: " + counter + " " + logLine);
				continue;
			}

			// Command type is located in second string
			switch(comSplit[1]) {
				case "camera":
					if (camera == null) camera = new CamTracker(comSplit);
					else camera.Update(comSplit);
					break;
				case "rCont":
					// discard
					break;
				case "lCont":
					// discard
					break;
				case "startHover":
					hoverCount_++;
					break;
				case "endHover":
					// don't know what to do with this yet
					break;
				case "scroll":
					// This is the format [time],scroll,[start/end],[scroll_pos]
					// however, it doesn't seem paired right, start/end aren't matched
					// Also, don't know what doc is scrolled
					break;
				case "startGrab":
					// Format:
					// [time],startGrab,[document name],[posx],[posy],[posz],[quatx],
					// [quaty],[quatz],[quatw],[scalex],[scaley],[scalez]
					DocumentTracker temp;
					if (docs_ == null) docs_ = new List<DocumentTracker>();
					// Find the document
					temp = docs_.Find(x => x.name.Equals(comSplit[2]));
					// update it if found
					if (temp != null) temp.Update(comSplit);
					// otherwise create new document tracker
					else {
						temp = new DocumentTracker(comSplit);
						docs_.Add(temp);
					}

					// increment grab counter for that object, too
					temp.grabCount++;
					break;
				case "grabbed":
				case "endGrab":
					// both grabbed and endGrab are handled the same way
					// theoretically, if an object is grabbed, it has been created
					DocumentTracker tempGrab;
					// Find the correct Object to update and do that
					tempGrab = docs_.Find(x => x.name.Equals(comSplit[2]));
					tempGrab.Update(comSplit);
					break;
				case "startHighlight":
					// both this and endHighlight are:
					// [time],[command],[document],[index]
					highlightCount_++;
					break;
				case "endHighlight":
					// not sure what to do here, so leaving it empty
					break;
				case "permHighlight":
					// format
					// [time],permHighlight,[document],"[highlighted section]"
					// calculate
					if (permHighlights_ == null) 
						permHighlights_ = new List<string>();
					permHighlights_.Add(comSplit[3]);
					break;
				case "showNote":
				case "hideNote":
					// format
					// [time],showNote,[document]
					break;
				case "copyDoc":
					// format
					// [time],copyDoc,[document]
					if (copyList_ == null) copyList_ = new List<string>();
					// originally wanted to not keep duplicates of copies
					// however, that would hide data.
					copyList.Add(comSplit[2]);
					break;
				case "search":
					// format
					// [time],search,[search string]
					searchCount_++;
					break;
				default:
					//Console.WriteLine(comSplit[1]);
					if (!(newComs.Contains(comSplit[1]))) {
						Console.WriteLine("new log: " + comSplit[1]);
						newComs.Add(comSplit[1]);
					}
					break;
			}
		}
	}

	public string summary() {
		string sum = "";

		sum += name_ + ",Document,Travel Distance,Times Grabbed";
		sum += "\n" + name_ + ",Camera," + camera.distTraveled.ToString("0.00");

		double totDist = 0f;
		int totGrab = 0;

		for (int i = 0; i < docs_.Count; i++) {
			sum += "\n" + name_ + "," + docs_[i].name + "," +
				docs_[i].distTraveled.ToString("0.00") + ","
				+ docs_[i].grabCount;
			totDist += docs_[i].distTraveled;
			totGrab += docs_[i].grabCount;
		}

		// all docs
		sum += "\n" + name_ + ",All Docs," + totDist.ToString("0.00") + "," + totGrab;
		sum += "\n" + name_ + ",Average," + (totDist / docs_.Count).ToString("0.00") + 
			"," + ((float)(totGrab) / docs_.Count).ToString("0.00");

		// highlight info
		if (permHighlights_ == null) {
			sum += "\n" + name_ + ",No Highlights";
		}
		else {
			sum += "\n" + name_ + ",Highlight Count," + permHighlights_.Count;
			double totHighSize = 0f;
			for (int i = 0; i < permHighlights_.Count; i++) {
				totHighSize += permHighlights_[i].Length;
			}
			sum += "\n" + name_ + ",Average Highlight," +
				(totHighSize / permHighlights_.Count).ToString("0.00");
		}
		return sum;
	}
}
