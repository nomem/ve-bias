# see hawkeslib for documentation for result interpretation
# https://readthedocs.org/projects/hawkeslib/downloads/pdf/stable/

import pandas
import numpy as np
from math import sqrt
from hawkeslib.model.mv_exp import MultivariateExpHawkesProcess as MVHP
from hawkeslib.model.uv_exp import UnivariateExpHawkesProcess as UVHP


df = pandas.read_csv('data/movement/camera_hawkes.csv')


movement_threshold = 0.4
print "We only consider movement greater than %f as an event" %movement_threshold
print "User#\tMain Movement\tAverage Subse-\tSubsequent Move-"
print "\tevery (sec)\tquent Movement\tment Delay"

for user in df['user'].unique():
    if user == 21 or user == 23:
        continue
    df_temp = df.loc[df['user']==user]
    pos_x = df_temp['pos_x'].tolist()
    pos_z = df_temp['pos_z'].tolist()
    dist_moved = [sqrt((pos_x[i]-pos_x[i-1])**2 + (pos_z[i]-pos_z[i-1])**2) if i != 0 else 0 for i in range(len(pos_x))]
    dist_moved = np.array(dist_moved)
    # print(dist_moved)
    # univariate
    time_uni = np.array(range(len(pos_x)))[dist_moved > movement_threshold].astype(np.float64)
    uv = UVHP()
    uv.fit(time_uni, method="em")
    m, a, at = uv.get_params()
    print "%d\t%d\t\t%.2f\t\t%.2f" %(user, round(1/m), a, 1/at)
    # multivariate
    # mv = MVHP()
    # mv.fit(np.array(range(len(pos_x))), dist_moved)
    # print mv.get_params()

